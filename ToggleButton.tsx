import React, { useState } from 'react';

// ToggleButton component
const ToggleButton: React.FC<{ options: string[]; onChange: (option: string) => void }> = ({ options, onChange }) => {
  const [selectedOption, setSelectedOption] = useState<string>(options[0]);

  // Handle click event on the buttons
  const handleClick = (option: string) => {
    setSelectedOption(option);
    onChange(option);
  };

  return (
    <div>
      {options.map(option => (
        <button
          key={option}
          onClick={() => handleClick(option)}
          style={{
            marginRight: '8px',
            backgroundColor: option === selectedOption ? 'lightblue' : 'transparent',
          }}
        >
          {option}
        </button>
      ))}
    </div>
  );
};

export default ToggleButton;
