enum TextAlign {
  Left = "left",
  Center = "center",
  Right = "right",
  Justify = "justify"
}

const alignmentToggle = document.getElementById('alignmentToggle');
const textContent = document.getElementById('textContent');

if (alignmentToggle && textContent) {
  alignmentToggle.addEventListener('click', (event) => {
    const target = event.target as HTMLButtonElement;
    if (target.tagName === 'BUTTON') {
      const alignment = target.getAttribute('data-alignment');
      if (alignment) {
        setTextAlignment(alignment as TextAlign);
        setActiveButton(target);
      }
    }
  });
}

function setTextAlignment(alignment: TextAlign) {
  if (textContent) {
    textContent.style.textAlign = alignment;
  }
}

function setActiveButton(button: HTMLButtonElement) {
  const buttons = Array.prototype.slice.call((alignmentToggle as HTMLElement).querySelectorAll('button'));
  buttons.forEach(btn => {
    btn.classList.remove('active');
  });
  button.classList.add('active');
}
