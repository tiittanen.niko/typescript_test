var TextAlign;
(function (TextAlign) {
    TextAlign["Left"] = "left";
    TextAlign["Center"] = "center";
    TextAlign["Right"] = "right";
    TextAlign["Justify"] = "justify";
})(TextAlign || (TextAlign = {}));
var alignmentToggle = document.getElementById('alignmentToggle');
var textContent = document.getElementById('textContent');
if (alignmentToggle && textContent) {
    alignmentToggle.addEventListener('click', function (event) {
        var target = event.target;
        if (target.tagName === 'BUTTON') {
            var alignment = target.getAttribute('data-alignment');
            if (alignment) {
                setTextAlignment(alignment);
                setActiveButton(target);
            }
        }
    });
}
function setTextAlignment(alignment) {
    if (textContent) {
        textContent.style.textAlign = alignment;
    }
}
function setActiveButton(button) {
    var buttons = Array.prototype.slice.call(alignmentToggle.querySelectorAll('button'));
    buttons.forEach(function (btn) {
        btn.classList.remove('active');
    });
    button.classList.add('active');
}
