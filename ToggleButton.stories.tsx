import React from 'react';
import { Meta } from '@storybook/react';
import ToggleButton from './ToggleButton';

export default {
  title: 'Components/ToggleButton',
  component: ToggleButton,
} as Meta;

export const Default = () => {
  const handleOptionChange = (option: string) => {
    console.log('Selected option:', option);
    // Do something with the selected option
  };

  return <ToggleButton options={['Option 1', 'Option 2', 'Option 3', 'Option 4']} onChange={handleOptionChange} />;
};
